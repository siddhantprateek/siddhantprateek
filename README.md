<!-- ![banner](https://i.imgur.com/Ww2nuXA.png) -->



<p style="clear:both;">
<h1><img src = "https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="35px"  height="35px"/>  Hi, I’m Siddhant </h1><img src="https://github.com/siddhantprateek/siddhantprateek/blob/main/coder2.gif" align="left" height="400vh" margin="20rem">

I'm a open source enthusiast. Love to learn from communities. Believe in power of community. Love to share knowledge what I have learned. Apart from that, I'm more into Blockchain, cloud native and DevOps related work. Looking forward to collabrate. 🚀
</p>

<!-- ![68889-deliverame-app](https://user-images.githubusercontent.com/43869046/126028403-244771ba-f18a-46a9-a557-bafc0d2bd313.gif) -->
<!-- <img src="https://user-images.githubusercontent.com/43869046/126028403-244771ba-f18a-46a9-a557-bafc0d2bd313.gif" align="left" height="500vh"> -->

<p>
<h2>📫 How to reach me through<img src='https://raw.githubusercontent.com/ShahriarShafin/ShahriarShafin/main/Assets/handshake.gif' width="35px"  height="35px" >
</p></h2>
<!-- Socials Links and Badges -->
<p >
	<a href="https://www.linkedin.com/in/siddhantprateek/">
		<img src="https://img.icons8.com/fluent/48/000000/linkedin.png"/>
	</a>
	<a href="https://twitter.com/siddhantprateek">
		<img src="https://img.icons8.com/color/48/000000/twitter-circled--v5.png" 
	 height="48" style="vertical-align:top; margin:4px"/>
	</a>
	<a href="https://www.facebook.com/siddhant.prateek.7/">
		<img src="https://img.icons8.com/fluent/48/000000/facebook-new.png"/>
	</a>
	<a href="https://medium.com/@siddhantprateek">
		<img src="https://img.icons8.com/ios-filled/50/000000/medium-monogram--v2.png"
	height="48" style="vertical-align:top; margin:4px"/>
	</a>
	<a href="https://devpost.com/siddhantprateek?ref_content=user-portfolio&ref_feature=portfolio&ref_medium=global-nav">
		<img src="https://img.icons8.com/color/48/000000/devpost.png"/>
	</a>
	<a href="https://dev.to/siddhantprateek">
		<img src="https://img.icons8.com/windows/64/000000/dev.png"
	height="48" style="vertical-align:top; margin:4px"/>
	</a>
	<a href="https://www.behance.net/siddhantprateek">
		<img src="https://img.icons8.com/color/48/000000/behance.png"/>
	</a>
</p>
<br/><br/>
<!-- Language and Tools -->
<h2>Languages & Tools that i use...<img src = "https://media2.giphy.com/media/QssGEmpkyEOhBCb7e1/giphy.gif?cid=ecf05e47a0n3gi1bfqntqmob8g9aid1oyj2wr3ds3mg700bl&rid=giphy.gif" width="35px"  height="35px"> </h2>

| |   |
| -------- | -------- | 
| **Programming Languages**     | ![C](https://img.shields.io/badge/c-%2300599C.svg?style=for-the-badge&logo=c&logoColor=white)![C++](https://img.shields.io/badge/c++-%2300599C.svg?style=for-the-badge&logo=c%2B%2B&logoColor=white)![Go](https://img.shields.io/badge/go-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white)![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)     | 
| **DevOps Tools**     | ![Kubernetes](https://img.shields.io/badge/kubernetes-%23326ce5.svg?style=for-the-badge&logo=kubernetes&logoColor=white)![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)![Prometheus](https://img.shields.io/badge/Prometheus-E6522C?style=for-the-badge&logo=Prometheus&logoColor=white)![Grafana](https://img.shields.io/badge/grafana-%23F46800.svg?style=for-the-badge&logo=grafana&logoColor=white)     | 
| **Blockchain Tools** | ![Hyperledger](https://img.shields.io/badge/hyperledger-2F3134?style=for-the-badge&logo=hyperledger&logoColor=white)![Ethereum](https://img.shields.io/badge/Ethereum-3C3C3D?style=for-the-badge&logo=Ethereum&logoColor=white)![Solidity](https://img.shields.io/badge/Solidity-%23363636.svg?style=for-the-badge&logo=solidity&logoColor=white)|
|**Testing Tools** | ![cypress](https://img.shields.io/badge/-cypress-%23E5E5E5?style=for-the-badge&logo=cypress&logoColor=058a5e)![Mocha](https://img.shields.io/badge/-mocha-%238D6748?style=for-the-badge&logo=mocha&logoColor=white)|
| **Backend Framework & Tools** | ![Flask](https://img.shields.io/badge/flask-%23000.svg?style=for-the-badge&logo=flask&logoColor=white)![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)![NestJS](https://img.shields.io/badge/nestjs-%23E0234E.svg?style=for-the-badge&logo=nestjs&logoColor=white)![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?style=for-the-badge&logo=graphql&logoColor=white)![Apollo-GraphQL](https://img.shields.io/badge/-ApolloGraphQL-311C87?style=for-the-badge&logo=apollo-graphql) |
|  **Frontend Framework** | ![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)![React Native](https://img.shields.io/badge/react_native-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)![Next JS](https://img.shields.io/badge/Next-black?style=for-the-badge&logo=next.js&logoColor=white)| 
| **Basic tools** | ![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)![Figma](https://img.shields.io/badge/figma-%23F24E1E.svg?style=for-the-badge&logo=figma&logoColor=white)![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)|


### My Blogs

- [All About Envoy Proxy](https://siddhantprateek.space/all-about-envoy-proxy)
- [History of RedHat & Inside RedHat's Open Source Community](https://siddhantprateek.space/history-of-redhat-and-inside-redhats-open-source-community)
- [Docker and How to dockerize React App](https://siddhantprateek.space/docker-and-how-to-dockerize-react-app)
- [Apache SkyWalking and It's Monitoring UI](https://blog.goupaz.com/posts/post07/apache-skywalking-and-its-monitoring-ui/)
- [What are the requirements required to run Medusa?](https://aviyel.com/post/3238/what-are-the-requirements-required-to-run-medusa)
- [How to Deploy Docz in Netlify?](https://aviyel.com/post/2891/how-to-deploy-docz-in-netlify)


| | |
| -------- | -------- |
|<img width="100%" src="https://github-readme-stats.vercel.app/api?username=siddhantprateek&show_icons=true&theme=nord" /> | <img width="100%" src="https://github-readme-streak-stats.herokuapp.com/?user=siddhantprateek&theme=nord" /> |

---

<!-- <div align="center">
   <a href="https://github.com/siddhantprateek">
     <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=siddhantprateek&theme=vue-dark&hide_langs_below=1" />
   </a>
</div>
 -->
<!-- Conritbution Graph -->
<details>
  <summary>Contribution Graph</summary>

[![Siddhant Prateek's github activity graph](https://activity-graph.herokuapp.com/graph?username=siddhantprateek&theme=github)](https://github.com/siddhantprateek/github-readme-activity-graph)
</details>

<p align="left"> <img src="https://komarev.com/ghpvc/?username=siddhantprateek&label=Profile%20views&color=0e75b6&style=flat" alt="siddhantprateek" /> </p>
